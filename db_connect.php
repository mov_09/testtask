<?
	$host = 'localhost';
	$dbname = 'reviews';
	$user = 'root';
	$pass = '';
	$options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ];
	try {
	    $dbh = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $user, $pass, $options);
	} catch (PDOException $e) {
	    echo "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}