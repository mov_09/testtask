<?
	date_default_timezone_set('Europe/Kiev');

	require_once 'db_connect.php';

	$image_path = '';
	if (isset($_FILES['image']['type']) && $_FILES['image']['type'] != ''):
		$valid_extensions = array('jpeg', 'jpg', 'png');
		$temporary = explode(".", $_FILES["image"]["name"]);
		$image_extension = end($temporary);
		if ((($_FILES['image']['type'] == 'image/png') || ($_FILES['image']['type'] == 'image/jpg') || ($_FILES['image']['type'] == 'image/jpeg')) && ($_FILES['image']['size'] < 500000)	&& in_array($image_extension, $valid_extensions)):
			if ($_FILES['image']['error'] > 0):
				$error_mess = $_FILES['image']['error'];
			else:
				$image_path = 'uploads/' . date('mdYhis', time()) . '.' . $image_extension;
				move_uploaded_file($_FILES['image']['tmp_name'], $image_path) ;
			endif;
		else:
			$error_mess = 'Invalid file size or type';
		endif;
	endif;

	if (!isset($error_mess)):
		if (validation()):
			$stm = $dbh->prepare('INSERT INTO reviews (name, email, review, image) VALUES (:name, :email, :review, :image)');
			$stm->bindParam(':name', $_REQUEST['name']);
			$stm->bindParam(':email', $_REQUEST['email']);
			$stm->bindParam(':review', strip_tags(html_entity_decode($_REQUEST['review'])));
			$stm->bindParam(':image', $image_path);
		    $stm->execute();
		    $success_mess = 'Thank you! Your review sended.';
		else:
			$error_mess = 'Error at validation. Please check fields.';
		endif;
	endif;

	if (!isset($error_mess)):
		echo $success_mess;
	else:
		echo $error_mess;
	endif;

    function validation() {
    	if (!empty($_REQUEST['email']) && !empty($_REQUEST['review']))
    		return true;
    	else
    		return false;
    }