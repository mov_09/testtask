<?
	require_once 'db_connect.php';

	$error_mess = "Something's gone wrong.";
	if (isset($_REQUEST['id']) && isset($_REQUEST['goal']) && !empty($_REQUEST['id']) && !empty($_REQUEST['goal'])):
		switch ($_REQUEST['goal']):
		    case "delete":
				$stm = $dbh->prepare("DELETE FROM reviews WHERE id = :id");
				$stm->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);   
				$stm->execute();
				$success_mess = 'Deleted.';
		        break;
		    case "publish":
		        $stm = $dbh->prepare("UPDATE reviews SET status = :status WHERE id = :id");
				$stm->bindParam(':status', $status = 2, PDO::PARAM_INT);   
				$stm->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);   
				$stm->execute();
				$success_mess = 'Updated.';
		        break;
		    case "reject":
		        $stm = $dbh->prepare("UPDATE reviews SET status = :status WHERE id = :id");
				$stm->bindParam(':status',  $status = 1, PDO::PARAM_INT);
				$stm->bindParam(':id', $_REQUEST['id'], PDO::PARAM_INT);   
				$stm->execute();
				$success_mess = 'Updated.';
		        break;
		endswitch;
	endif;
	if (isset($success_mess))
		echo $success_mess;
	else
		echo $error_mess;