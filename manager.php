<?
	ob_start();
	session_start();

	$logged = false;
	if (isset($_POST['login']) && !empty($_POST['user'])  && !empty($_POST['pass']) && $_POST['user'] == 'user' && $_POST['pass'] == '1111'):
		$_SESSION['logged'] = true;
		$logged = true;
    endif;
    if (isset($_SESSION['logged']) && $_SESSION['logged'])
    	$logged = true;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>	</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/general.css">

	<script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
	<div id="main" class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="review-wrapp" <?= ($logged) ? 'style="height: calc(100vh - 60px)"' : '' ?>>
					<ul class="review-list media-list" <?= (!$logged) ? 'style="height: calc(100vh - 180px)"' : '' ?>>
						<?
							if ($logged):
								require_once 'db_connect.php';

								$stmt = $dbh->prepare('SELECT * FROM reviews');
								$stmt->execute();

								while ($row = $stmt->fetch(PDO::FETCH_LAZY)):
						?>
						<li class="media review-list_item">
							<? 
								$image = $row['image'];
								$status = $row['status'];
								if ($image): 
							?>
							<div class="media-left">
								<a href="<?= $image ?>" target="_blank">
									<img class="media-object review-list_item_img" src="<?= $image ?>" alt="Static Alt">
								</a>
							</div>
							<?
								endif; 
							?>
							<div class="media-body">
								<h4 class="media-heading"><?= $row['name'] ?></h4>
								<p><?= $row['review'] ?></p>
								<div class="row">
									<div class="col-md-4  review-list_item_status">
										Status: <?= $status ?>
									</div>
									<div class="col-md-8 text-right">
										<? if ($status == 1 || $status == 3): ?>
										<button type="button" class="btn btn-default publishBtn">Publish</button>
										<? endif; ?>
										<? if ($status == 2 || $status == 3): ?>
										<button type="button" class="btn btn-default rejectBtn">Reject</button>
										<? endif; ?>
										<button type="button" class="btn btn-default deleteBtn">Delete</button>
										<input type="hidden" name="id" value="<?= $row['id'] ?>">
									</div>
								</div>
								
							</div>
						</li>
						<?
								endwhile;
						?>
						<script>
							$('.publishBtn').click(function(e) {
								e.preventDefault();
								$.ajax({
									url: 'manage_review.php',
									type: 'POST',
									data: 'goal=publish&id=' + $(this).parent().find('input[name="id"]').val(),
									success: function(message) {
										alert(message);
										location.reload()
									}
								});
							});
							$('.deleteBtn').click(function(e) {
								e.preventDefault();
								$.ajax({
									url: 'manage_review.php',
									type: 'POST',
									data: 'goal=delete&id=' + $(this).parent().find('input[name="id"]').val(),
									success: function(message) {
										alert(message);
										location.reload()
									}
								});
							});
							$('.rejectBtn').click(function(e) {
								e.preventDefault();
								$.ajax({
									url: 'manage_review.php',
									type: 'POST',
									data: 'goal=reject&id=' + $(this).parent().find('input[name="id"]').val(),
									success: function(message) {
										alert(message);
										location.reload()
									}
								});
							});
						</script>
						<?
							else:
								echo 'Need to login.';
							endif;
						?>
					</ul>
					<? if (!$logged): ?>
					<form action = "<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post" class="login-form">
						<div class="login-form_title">Admin login data</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control" name="user" placeholder="Login">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="password" class="form-control" name="pass"  placeholder="Password">
								</div>
							</div>
							<div class="col-md-4">
								<input type="hidden" name="login" value="1">
								<button type="submit" class="btn btn-block btn-default">Submit</button>
							</div>
						</div>
					</form>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>