<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>	</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/general.css">

	<script src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
	<div id="main" class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="review-wrapp">
					<ul class="review-list media-list">
						<?
							require_once 'db_connect.php';

							$stmt = $dbh->prepare('SELECT * FROM reviews WHERE status = ?');
							$stmt->execute([2]);
							$marker = false;
							while ($row = $stmt->fetch(PDO::FETCH_LAZY)):
								$marker = true;
						?>
						<li class="media review-list_item">
							<? 
								$image = $row['image'];
								if ($image): 
							?>
							<div class="media-left">
								<a href="<?= $image ?>" target="_blank">
									<img class="media-object review-list_item_img" src="<?= $image ?>" alt="Static Alt">
								</a>
							</div>
							<?
								endif; 
							?>
							<div class="media-body">
								<h4 class="media-heading"><?= $row['name'] ?></h4>
								<?= $row['review'] ?>
							</div>
						</li>
						<?
							endwhile;
							if (!$marker)
								echo '<li>List is empty.</li>';
						?>
					</ul>
					<form action="#" id="reviewForm" class="review-form">
						<div class="review-form_title">Add a review</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Email*">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="name"  placeholder="Name">
								</div>
							</div>
						</div>
						<div class="form-group">
							<textarea name="review" class="form-control review-form_textarea" placeholder="Your review*"></textarea>
						</div>
						<div class="form-group">
							<label for="image">Image</label>
							<input type="file" name="image" id="image">
							<p class="help-block">Only .png or .jpg formats allowed.</p>
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
					</form>
					<script>
						$('#reviewForm').submit(function(e) {
							e.preventDefault();
							$.ajax({
								url: 'add_review.php',
								type: 'POST',
								data: new FormData(this),
								contentType: false,
    	    					processData: false,
								success: function(message) {
									alert(message);
								}
							});
						})
					</script>
				</div>
			</div>
		</div>
	</div>
</body>
</html>